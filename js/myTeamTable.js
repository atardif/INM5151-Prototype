myTeamTable();

function myTeamTable() {

  var currentUser = JSON.parse(localStorage.getItem("currentUser"));
  var teams = JSON.parse(localStorage.getItem("teams"));

  if (currentUser && currentUser.teams.length > 0) {

    var myTableDiv = document.getElementById("my-team-table");

    var table = document.createElement('table');
    table.className = "table";

    var tableHeader = document.createElement('thead');

    var tr_head = document.createElement('tr');

    table.appendChild(tableHeader);

    tableHeader.appendChild(tr_head);

    var th_head_Nom = document.createElement('th');
    th_head_Nom.appendChild(document.createTextNode("Nom"));
    var th_head_Sport = document.createElement('th');
    th_head_Sport.appendChild(document.createTextNode("Sport"));
    var th_head_MembreEquipe = document.createElement('th');
    th_head_MembreEquipe.appendChild(document.createTextNode("Membre de l'equipe"));
    var th_head_Actions = document.createElement('th');
    th_head_Actions.className = "text-center";
    th_head_Actions.appendChild(document.createTextNode("Actions"));

    tr_head.appendChild(th_head_Nom);
    tr_head.appendChild(th_head_Sport);
    tr_head.appendChild(th_head_MembreEquipe);
    //tr_head.appendChild(th_head_Actions);

    var tableBody = document.createElement('tbody');
    table.appendChild(tableBody);


    for (var i = 0; i < currentUser.teams.length; i++) {
      if (teams && teams.length) {
        for (var z = 0; z < teams.length; z++) {
          if (teams[z].id === currentUser.teams[i]) {
            var tr_body = document.createElement('tr');
            tableBody.appendChild(tr_body);

            var titleTeam = teams[z].name;
            var sportType = getSportType(teams[z].id);

            var playersTable = "";
            for (var j = 0; j < teams[z].players.length; j++) {
              playersTable += teams[z].players[j] + " ";
            }


            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(titleTeam));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(getSportType(teams[z].id)));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(playersTable));
            tr_body.appendChild(td);

            var hrefConsulter = document.createElement('a');
            var hrefSupprimer = document.createElement('a');
            /** 
                    hrefConsulter.className = "btn btn-info btn-xs";
                    hrefConsulter.href = "#";
                    hrefConsulter.innerHTML = "Consulter";
                    hrefConsulter.style = "margin-right:5px;";
            
                    hrefSupprimer.className = "btn btn-danger btn-xs";
                    hrefSupprimer.href = "#";
                    hrefSupprimer.innerHTML = "Supprimer";
                    hrefSupprimer.addEventListener("click", function () { if (confirm("Êtes-vous sur?")) removeTeam(this.parentElement.parentElement) });
            
                    var td = document.createElement('td');
                    td.width = '75';
                    td.appendChild(hrefConsulter);
                    td.appendChild(hrefSupprimer);
                    td.className = "text-center";
                    tr_body.appendChild(td);
            */
          }
        }
      }
    }
    myTableDiv.appendChild(table);
  } else {
    var p = document.createElement('p');
    p.innerHTML = "Vous n'êtes présentement membre d'aucune équipe";
    var corp = document.getElementById("my-team-table");
    corp.appendChild(p);
  }

  return true;

}

function removeTeam(element) {
  deleteTeam(element.children[0].innerHTML)
  element.remove();
}