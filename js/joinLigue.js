populateSportList();
list_filtered_leagues();

function populateSportList() {
    let sports = sportsAvailableInLeagues();
    for (let sport of sports) {
        $('#list-sport').append(new Option(sport, sport, false, false));
    }
}

function list_filtered_leagues() {
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    let user = JSON.parse(localStorage.getItem("currentUser"));

    removeAllChilds("list-league");
    let type = document.getElementById("list-sport").value;
    let search = document.getElementById("champ-team").value;

    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i].sportType != type || !leagues[i].name.toLowerCase().includes(search.toLowerCase()) || leagues[i].toApprove.includes(user.id) || user.leagues.includes(leagues[i].id)) {
            continue;
        }
        $('#list-league').append(new Option(leagues[i].name, leagues[i].id, false, false));
    }
    return true;
}

function joinLeague() {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    let leagueId = document.getElementById("list-league").value;
    if (leagueId) {
        let leagues = JSON.parse(localStorage.getItem("leagues"));
        for (let i = 0; i < leagues.length; i++) {
            if (leagues[i].id == leagueId) {
                leagues[i].members.push(user.id);
                user.leagues.push(leagues[i].id);
            }
        }
        localStorage.setItem("leagues", JSON.stringify(leagues));	
        localStorage.setItem("currentUser", JSON.stringify(user));	
        return true;
    }
    return false;
}
