function userExist() {
    var username = document.getElementById("champ_username").value;
    var password = document.getElementById("champ_password").value;

    var users = JSON.parse(localStorage.getItem("users"));

    var invalid_user_msg = document.getElementById("invalid_user");
    invalid_user_msg.innerHTML = "";
    for (let index = 0; index < users.length; index++) {
        const user = users[index];
        if (user.id === username && user.password === password) {
            localStorage.setItem("currentUser", JSON.stringify(user));
            return true;
        }
    }

    invalid_user_msg.innerHTML = "L'utilisateur est invalide";
    return false;
}

function validateLogin() {
    var usernameIsValid = validateInput("champ_username");
    var passwordIsValid = validateInput("champ_password");

    if (usernameIsValid && passwordIsValid) {
        if (userExist()) {
            showButtons();
            return true;
        }
    }

    return false;
}

