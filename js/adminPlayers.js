managePlayersTable();

function managePlayersTable() {

  var users = JSON.parse(localStorage.getItem("users"));

  if (users.length > 0)  {

    var myTableDiv = document.getElementById("manage-players-table");

    var table = document.createElement('table');
    table.className = "table";

    var tableHeader = document.createElement('thead');
    
    var tr_head = document.createElement('tr');

    table.appendChild(tableHeader);

    tableHeader.appendChild(tr_head);

    var th_head_Id = document.createElement('th');
    th_head_Id.appendChild(document.createTextNode("ID"));
    var th_head_Psw = document.createElement('th');
    th_head_Psw.appendChild(document.createTextNode("Mot de passe"));

    var th_head_Actions = document.createElement('th');
    th_head_Actions.appendChild(document.createTextNode("Actions"));

    tr_head.appendChild(th_head_Id);
    tr_head.appendChild(th_head_Psw);   
    tr_head.appendChild(th_head_Actions);
    
    var tableBody = document.createElement('tbody');
    table.appendChild(tableBody);


    for (var i = 0; i < users.length; i++) {
      var tr_body = document.createElement('tr');
      tableBody.appendChild(tr_body);

      var userId = users[i].id;
      var userPsw = users[i].password;
    
      var td = document.createElement('td');
      td.width = '75';
      td.appendChild(document.createTextNode(userId));
      tr_body.appendChild(td);

      var td = document.createElement('td');
      td.width = '75';
      td.appendChild(document.createTextNode(userPsw));
      tr_body.appendChild(td);


      var hrefConsulter = document.createElement('a');
      var hrefSupprimer = document.createElement('a');

      hrefConsulter.className = "btn btn-success btn-xs";
      hrefConsulter.href = "#";
      hrefConsulter.innerHTML = "Gerer";
      hrefConsulter.style = "margin-right:5px;";

      hrefSupprimer.className = "btn btn-danger btn-xs";
      hrefSupprimer.href = "#";
      hrefSupprimer.innerHTML = "Retirer";

      var td = document.createElement('td');
      td.width = '75';
      td.appendChild(hrefConsulter);
      td.appendChild(hrefSupprimer);
      //td.className = "text-center";
      tr_body.appendChild(td);  
    }
    myTableDiv.appendChild(table);
  }else{
    var p = document.createElement('h2');
    p.innerHTML = "Liste des joueurs est vide";
    var corp = document.getElementById("manage-players-table");
    corp.appendChild(p);
  }

  return true;
  
}
