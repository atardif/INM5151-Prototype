activityTable();


function activityTable() {

    var activities = JSON.parse(localStorage.getItem("activities"));

    if (activities && activities.length > 0) {

        var myTableDiv = document.getElementById("activity-table");

        var table = document.createElement('table');
        table.className = "table";

        var tableHeader = document.createElement('thead');

        var tr_head = document.createElement('tr');

        table.appendChild(tableHeader);

        tableHeader.appendChild(tr_head);

        var th_head_Name = document.createElement('th');
        th_head_Name.appendChild(document.createTextNode("Name"))
        var th_head_Localisation = document.createElement('th');
        th_head_Localisation.appendChild(document.createTextNode("Localisation"));
        var th_head_startHour = document.createElement('th');
        th_head_startHour.appendChild(document.createTextNode("startHour"));
        var th_head_endHour = document.createElement('th');
        th_head_endHour.appendChild(document.createTextNode("endHour"));
        var th_head_date = document.createElement('th');
        th_head_date.appendChild(document.createTextNode("date"));
        var th_head_hostTeam = document.createElement('th');
        th_head_hostTeam.appendChild(document.createTextNode("hostTeam"));
        var th_head_guestTeam = document.createElement('th');
        th_head_guestTeam.appendChild(document.createTextNode("guestTeam"));

        tr_head.appendChild(th_head_Name);
        tr_head.appendChild(th_head_Localisation);
        tr_head.appendChild(th_head_startHour);
        tr_head.appendChild(th_head_endHour);
        tr_head.appendChild(th_head_date);
        tr_head.appendChild(th_head_hostTeam);
        tr_head.appendChild(th_head_guestTeam);
        if (currentUser && currentUser.id === "admin" ) {
            var tr_head_admin = document.createElement('th');
            tr_head_admin.className = "text-center";
            tr_head_admin.appendChild(document.createTextNode("Admin"));
            tr_head.appendChild(tr_head_admin);
        }
        //tr_head.appendChild(th_head_Actions);

        var tableBody = document.createElement('tbody');
        table.appendChild(tableBody);


        for (var i = 0; i < activities.length; i++) {
            var tr_body = document.createElement('tr');
            tableBody.appendChild(tr_body);


            var name = activities[i].name;
            var localisation = activities[i].localisation;
            var startHour = activities[i].startHour;
            var endHour = activities[i].endHour;
            var date = activities[i].date;
            var hostTeam = activities[i].equipes[0];
            var guestTeam = activities[i].equipes[1];

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(name));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(localisation));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(startHour));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(endHour));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(date));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(getTeamName(hostTeam)));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(getTeamName(guestTeam)));
            tr_body.appendChild(td);

            if (currentUser && currentUser.id === "admin" ) {
                var hrefSupprimer = document.createElement('a');
                hrefSupprimer.className = "btn btn-danger btn-xs";
                hrefSupprimer.href = "#";
                hrefSupprimer.innerHTML = "Supprimer";
                hrefSupprimer.addEventListener("click", function () { if (confirm("Êtes-vous sur?")); this.parentElement.parentElement.remove()});
                
                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(hrefSupprimer);
                td.className = "text-center";
                tr_body.appendChild(td);
            }
            /**
                            hrefConsulter.className = "btn btn-info btn-xs";
                            hrefConsulter.href = "#";
                            hrefConsulter.innerHTML = "Consulter";
                            hrefConsulter.style = "margin-right:5px;";
            
                            hrefSupprimer.className = "btn btn-danger btn-xs";
                            hrefSupprimer.href = "#";
                            hrefSupprimer.innerHTML = "Supprimer";
            
                            var td = document.createElement('td');
                            td.width = '75';
                            td.appendChild(hrefConsulter);
                            td.appendChild(hrefSupprimer);
                            td.className = "text-center";
                            tr_body.appendChild(td);
             */
        }
        myTableDiv.appendChild(table);
    } else {
        var p = document.createElement('p');
        p.innerHTML = "Aucune activité";
        var corp = document.getElementById("activity-table");
        corp.appendChild(p);
    }

    return true;
}

