myLeagueTable();


function myLeagueTable() {

  var currentUser = JSON.parse(localStorage.getItem("currentUser"));
  var leagues = JSON.parse(localStorage.getItem("leagues"));

  if (currentUser && currentUser.leagues.length > 0) {

    var myTableDiv = document.getElementById("my-league-table");

    var table = document.createElement('table');
    table.className = "table";

    var tableHeader = document.createElement('thead');

    var tr_head = document.createElement('tr');

    table.appendChild(tableHeader);

    tableHeader.appendChild(tr_head);

    var th_head_Titre = document.createElement('th');
    th_head_Titre.appendChild(document.createTextNode("Titre"));
    var th_head_Debut = document.createElement('th');
    th_head_Debut.appendChild(document.createTextNode("Debut"));
    var th_head_Fin = document.createElement('th');
    th_head_Fin.appendChild(document.createTextNode("Fin"));
    var th_head_NombreEquipe = document.createElement('th');
    th_head_NombreEquipe.className = "text-center";
    th_head_NombreEquipe.appendChild(document.createTextNode("Nombre Equipe"));
    var th_head_JoueursEquipe = document.createElement('th');
    th_head_JoueursEquipe.className = "text-center";
    th_head_JoueursEquipe.appendChild(document.createTextNode("Joueurs Equipe"));
    var th_head_Actions = document.createElement('th');
    th_head_Actions.className = "text-center";
    th_head_Actions.appendChild(document.createTextNode("Actions"));


    tr_head.appendChild(th_head_Titre);
    tr_head.appendChild(th_head_Debut);
    tr_head.appendChild(th_head_Fin);
    tr_head.appendChild(th_head_NombreEquipe);
    tr_head.appendChild(th_head_JoueursEquipe);
    tr_head.appendChild(th_head_Actions);

    var tableBody = document.createElement('tbody');
    table.appendChild(tableBody);

    for (var i = 0; i < currentUser.leagues.length; i++) {
      for (var j = 0; j < leagues.length; j++) {
        if (leagues[j].id !== currentUser.leagues[i])
          continue;
        var tr_body = document.createElement('tr');
        tr_body.className = leagues[j].id;
        tableBody.appendChild(tr_body);

        var title = leagues[j].name;
        var seasonStart = leagues[j].seasonStart;
        var seasonEnd = leagues[j].seasonEnd;
        var nbTeams = leagues[j].nbTeams;
        var nbPlayerByTeams = leagues[j].nbPlayersByTeams;


        var td = document.createElement('td');
        td.width = '75';
        td.appendChild(document.createTextNode(title));
        tr_body.appendChild(td);

        var td = document.createElement('td');
        td.width = '75';
        td.appendChild(document.createTextNode(seasonStart));
        tr_body.appendChild(td);

        var td = document.createElement('td');
        td.width = '75';
        td.appendChild(document.createTextNode(seasonEnd));
        tr_body.appendChild(td);

        var td = document.createElement('td');
        td.width = '75';
        td.appendChild(document.createTextNode(nbTeams));
        td.className = "text-center";
        tr_body.appendChild(td);

        var td = document.createElement('td');
        td.width = '75';
        td.appendChild(document.createTextNode(nbPlayerByTeams));
        td.className = "text-center";
        tr_body.appendChild(td);

        var hrefConsulter = document.createElement('a');
        var hrefSupprimer = document.createElement('a');

        hrefConsulter.className = "btn btn-info btn-xs my-2";
        hrefConsulter.href = "ligueConsult.html";
        hrefConsulter.innerHTML = "Consulter";
        hrefConsulter.style = "margin-right:5px;";
        hrefConsulter.addEventListener("click", function () { localStorage.setItem("currentLeague", this.parentElement.parentElement.className) });

        hrefSupprimer.className = "btn btn-danger btn-xs my-2";
        hrefSupprimer.href = "#";
        hrefSupprimer.innerHTML = "Supprimer";
        hrefSupprimer.addEventListener("click", function () { if (confirm("Êtes-vous sur?")); this.parentElement.parentElement.remove() });

        
        var td = document.createElement('td');
        td.style = "text-align:center;";
        td.width = '75';
        td.appendChild(hrefConsulter);
        if(currentUser.id === leagues[j].owner)
          td.appendChild(hrefSupprimer);
        tr_body.appendChild(td);

      }
    }
    myTableDiv.appendChild(table);
  } else {
    var p = document.createElement('p');
    p.innerHTML = "Vous ne faites partie d'aucune ligue";
    var corp = document.getElementById("my-league-table");
    corp.appendChild(p);
  }

  return true;
}
