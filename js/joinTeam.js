populateTeams();

function leagueForTeam(teamId) {
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i]["teams"].includes(teamId)) {
            return leagues[i];
        }
    }
    return null;
}

function populateTeams() {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    let userLeagues = leagues.filter(league => league["members"].includes(user.id));
    let teams = JSON.parse(localStorage.getItem("teams"));
    let availableTeams = teams.filter(
        team => userLeagues.some(
            league => league["teams"].includes(team.id)
        )
    );

    removeAllChilds("list-team");

    for (let i = 0; i < availableTeams.length; i++) {
        let team = availableTeams[i];
        if (!team["players"].includes(user.id)) {
            let league = leagueForTeam(team["id"]);
            let name = "Équipe: " + team["name"] + " de la  ligue " + league["name"] + " (" + league["sportType"] + ")";
            $('#list-team').append(new Option(name, team["id"], false, false));
        }
    }
    return true;
}

function joinTeam() {
    let user = JSON.parse(localStorage.getItem("currentUser"));
    let teams = JSON.parse(localStorage.getItem("teams"));
    let teamId = Number(document.getElementById("list-team").value);
    
    if (teamId) {
        // add user to team
        for (let i = 0; i < teams.length; i++) {
            if (teams[i].id === teamId) {
                teams[i]["players"].push(user.id);
                localStorage.setItem("teams", JSON.stringify(teams));
                break;
            }
        }

        // add team to user
        currentUser["teams"].push(teamId);
        localStorage.setItem("currentUser", JSON.stringify(currentUser));

        listUsers = JSON.parse(localStorage.getItem("users"));
        for (let i = 0; i < listUsers.length; i++) {
            if (listUsers[i].id === currentUser.id) {
                listUsers[i]["teams"].push(teamId);
            }
        }
        localStorage.setItem("users", JSON.stringify(listUsers));

        return true;
    }
    return false;
}
