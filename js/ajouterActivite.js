loadLeagues();
loadTeamsInSelectedLeague();

function loadLeagues() {
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i]["owner"] === currentUser.id) {
            $('#list-league').append(new Option(leagues[i].name, leagues[i].id, false, false));
        }
    }
}

function loadTeamsInSelectedLeague() {
    let currentLeagueId = Number($("#list-league").val());
    let allTeams = JSON.parse(localStorage.getItem("teams"));
    removeAllChilds("equipeHote");
    removeAllChilds("equipeVisiteur");

    let league = getLeague(currentLeagueId);
    for (let i = 0; i < league["teams"].length; i++) {
        for (let j = 0; j < allTeams.length; j++) {
            let team = allTeams[j];
            if (team.id === league["teams"][i]) {
                $('#equipeHote').append(new Option(team["name"], team["id"], false, false));
                $('#equipeVisiteur').append(new Option(team["name"], team["id"], false, false));
            }
        }
    }
}

$('#createActivityForm').submit(function validateActivite(event) {
    let idUnique = idIsUnique();
    let localisationIsValid = validateInput("localisation");
    let nameIsValid = validateInput("name");
    let hoursIsValid = validateHours();
    let teamsIsValid = validateTeams();

    if (idUnique && localisationIsValid && hoursIsValid && nameIsValid && teamsIsValid) {
        addActivite();
        return true;
    }

    return false;
});

function validateInput(input_id) {
    if (!input_id) {
        return false;
    }

    let input = document.getElementById(input_id);

    if (!input.value) {
        input.className = input.className + ' is-invalid'
        return false;
    }

    input.classList.remove('is-invalid');
    return true;
}

function validateHours() {
    let startHour = document.getElementById("heureDepart");
    let endHour = document.getElementById("heureFin");

    if (startHour.value.substring(0, 2) >= endHour.value.substring(0, 2)) {
        startHour.className = startHour.className + ' is-invalid'
        return false;
    }

    startHour.classList.remove('is-invalid');
    return true;
}

function validateTeams() {
    let hostTeam = document.getElementById("equipeHote");
    let guestTeam = document.getElementById("equipeVisiteur");

    if (hostTeam.value === guestTeam.value) {
        hostTeam.className = hostTeam.className + ' is-invalid'
        return false;
    }

    hostTeam.classList.remove('is-invalid');
    return true;
}

function addActivite() {
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let activityId = Number(document.getElementById("activityId").value);
    let name = document.getElementById("name").value;
    let localisation = document.getElementById("localisation").value;
    let startHour = document.getElementById("heureDepart").value;
    let endHour = document.getElementById("heureFin").value;
    let hostTeam = Number(document.getElementById("equipeHote").value);
    let guestTeam = Number(document.getElementById("equipeVisiteur").value);
    let date = document.getElementById("dateActivite").value;
    let activity = new Activity(
        activityId, localisation, date, startHour, endHour,
        [hostTeam, guestTeam], currentUser.id, currentUser.id, name
    );

    // update activities
    let activities = JSON.parse(localStorage.getItem("activities"));
    activities.push(activity)
    localStorage.setItem("activities", JSON.stringify(activities));

    // update league
    let currentLeagueId = Number($("#list-league").val());
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        let league = leagues[i];
        if (league["id"] === currentLeagueId) {
            league["activities"].push(activity.id);
            leagues[i] = league;
            break;
        }
    }
    localStorage.setItem("leagues", JSON.stringify(leagues));
}
