function acceptMember (id) {
	var ligues = JSON.parse(localStorage.getItem("leagues"));
	for (var i = 0; i < ligues.length; i++)
		if (ligues[i].id == localStorage.getItem("currentLeague"))
			break;
	ligues[i].toApprove = ligues[i].toApprove.filter(e => e !== id);
	ligues[i].members.push(id);
	localStorage.setItem("leagues",  JSON.stringify(ligues));
}

function refuseMember (id) {
	var ligues = JSON.parse(localStorage.getItem("leagues"));
	for (var i = 0; i < ligues.length; i++)
		if (ligues[i].id == localStorage.getItem("currentLeague"))
			break;
	ligues[i].toApprove = ligues[i].toApprove.filter(e => e !== id);
	localStorage.setItem("leagues",  JSON.stringify(ligues));
}