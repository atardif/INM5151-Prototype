var currentUser = JSON.parse(localStorage.getItem("currentUser"));
var users;

loadData();
showButtons();
showPages();

// fonction pour loader les données (utlisateurs/équipes) si premier utilisation du site avec un browser.
function loadData() {

    users = JSON.parse(localStorage.getItem("users"));

    // console.log(users);

    if (!users || !users.length) {

        localStorage.setItem("users",
            JSON.stringify([
                {
                    id: "francis",
                    password: "grondin",
                    teams: [1, 2],
                    leagues: [1, 3, 4]
                },
                {
                    id: "mohamed",
                    password: "bgd",
                    teams: [1, 5],
                    leagues: [1, 2, 3, 4]
                },
                {
                    id: "charles",
                    password: "qwerty",
                    teams: [10],
                    leagues: [3, 4]
                },
                {
                    id: "xavier",
                    password: "12345",
                    teams: [11, 12],
                    leagues: [4]
                }
            ]));


        localStorage.setItem("leagues",
            JSON.stringify([
                {
                    "id": 1,
                    "name": "Ligue des rattons",
                    "nbPlayersByTeams": 45,
                    "nbTeams": 2,
                    "seasonEnd": "2018-07-31",
                    "seasonStart": "2018-07-31",
                    "sportType": "beach volleyball",
                    "members": ["francis"],
                    "activities": [5, 6, 7, 9],
                    "teams": [5],
                    "owner": "francis",
                    "toApprove": ["mohamed"]
                },
                {
                    "id": 2,
                    "name": "Ligue des requins",
                    "nbPlayersByTeams": 40,
                    "nbTeams": 4,
                    "seasonEnd": "2017-07-31",
                    "seasonStart": "2019-07-31",
                    "sportType": "hockey cosom",
                    "members": ["mohamed"],
                    "activities": [8],
                    "teams": [1, 2],
                    "owner": "mohamed",
                    "toApprove": []
                },
                {
                    "id": 3,
                    "name": "League of Graphs",
                    "nbPlayersByTeams": 5,
                    "nbTeams": 8,
                    "seasonEnd": "2019-01-01",
                    "seasonStart": "2019-03-01",
                    "sportType": "League of Legends",
                    "members": ["francis", "mohamed", "charles"],
                    "activities": [],
                    "teams": [10],
                    "owner": "charles",
                    "toApprove": []
                },
                {
                    "id": 4,
                    "name": "Games of Jones",
                    "nbPlayersByTeams": 20,
                    "nbTeams": 30,
                    "seasonEnd": "2019-04-01",
                    "seasonStart": "2019-12-31",
                    "sportType": "Football américain",
                    "members": ["francis", "mohamed", "charles", "xavier"],
                    "activities": [300],
                    "teams": [11, 12],
                    "owner": "francis",
                    "toApprove": []
                }
            ]));

        localStorage.setItem("teams",
            JSON.stringify([
                {
                    "id": 1,
                    "name": "Équipe des rattons",
                    "players": ["francis", "mohamed"],
                    "owner": "francis"
                },
                {
                    "id": 2,
                    "name": "Les rebelles",
                    "players": ["xavier", "francis"],
                    "owner": "xavier"
                },
                {
                    "id": 5,
                    "name": "Équipe des requins",
                    "players": ["mohamed"],
                    "owner": "mohamed"
                },
                {
                    "id": 10,
                    "name": "Cloud 9",
                    "players": ["charles"],
                    "owner": "charles"
                },
                {
                    "id": 11,
                    "name": "Cowboys",
                    "players": ["francis", "mohamed", "xavier"],
                    "owner": "xavier"
                },
                {
                    "id": 12,
                    "name": "Patriots",
                    "players": ["charles", "xavier"],
                    "owner": "charles"
                }
            ])
        );

        localStorage.setItem("activities",
            JSON.stringify([
                {
                    "id": 1,
                    "localisation": "Saint-Sauveur",
                    "date": "2018-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [1, 2],
                    "confirmed": ["francis"],
                    "owner": "francis",
                    "name": "Rattons vs Rebelles"
                },
                {
                    "id": 5,
                    "localisation": "Mirabelle",
                    "date": "2018-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [1, 2],
                    "confirmed": ["francis"],
                    "owner": "francis",
                    "name": "Ratton vs Rebelles"
                },
                {
                    "id": 6,
                    "localisation": "Terrebonne",
                    "date": "2018-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [1, 2],
                    "confirmed": ["francis"],
                    "owner": "francis",
                    "name": "Ratton vs Rebelles"
                },
                {
                    "id": 7,
                    "localisation": "Québec",
                    "date": "2018-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [11, 12],
                    "confirmed": ["xavier"],
                    "owner": "xavier",
                    "name": "Cowboys vs Patriots"
                },
                {
                    "id": 8,
                    "localisation": "Montréal",
                    "date": "2018-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [11, 12],
                    "confirmed": ["xavier"],
                    "owner": "xavier",
                    "name": "Cowboys vs Patriots"
                },
                {
                    "id": 9,
                    "localisation": "plage Oka",
                    "date": "2019-07-13",
                    "startHour": "15:40",
                    "endHour": "16:20",
                    "equipes": [],
                    "confirmed": [],
                    "owner": "francis",
                    "name": "Partie de volleyball"
                },
                {
                    "id": 300,
                    "localisation": "Mont-Royal",
                    "date": "2019-07-14",
                    "startHour": "15:40",
                    "endHour": "16:40",
                    "equipes": [],
                    "confirmed": [],
                    "owner": "francis",
                    "name": "Partie de football"
                }
            ]));

    }
}

function logout() {
    currentUser = "";
    localStorage.setItem("currentUser", JSON.stringify(currentUser));
    window.location.href = "accueil.html";
    showButtons();
}

//fonction pour afficher les buttons de connexion selon le currentUser
function showButtons() {
    var loginButton = document.getElementById("login_button");
    var registerButton = document.getElementById("register_button");
    var logoutButton = document.getElementById("logout_button");

    if (currentUser) {
        loginButton.hidden = true;
        registerButton.hidden = true;
        logoutButton.hidden = false;
    } else {
        loginButton.hidden = false;
        registerButton.hidden = false;
        logoutButton.hidden = true;
    }
}

function showPages() {

    var monProfil = document.getElementById("mon_profil");
    var pseudo = document.getElementById("id-pseudo");

    if (currentUser) {
        monProfil.hidden = false;
        pseudo.hidden = false;
    } else {
        monProfil.hidden = true;
        pseudo.hidden = true;
    }
}

// fonction pour valider un input .form-control avec un div/span .invalid-feedback
function validateInput(input_id) {
    if (!input_id) {
        return false;
    }

    let input = document.getElementById(input_id);

    if (!input.value) {
        input.className = input.className + ' is-invalid'
        return false;
    }

    input.classList.remove('is-invalid');
    return true;
}

// function pour feed les options d'un select avec une liste de string.
function generateSelectOptions(items, selectId) {
    var select = document.getElementById(selectId);
    while (select.length > 0) {
        select.remove(select.length - 1);
    }

    items.forEach(function (item) {
        var option = document.createElement('option');
        select.appendChild(option);

        option.innerHTML += item;
    });
}


function getActivities(user) {
    var activities = [];
    var allActivities = JSON.parse(localStorage.getItem("activities"));
    var leagues = JSON.parse(localStorage.getItem("leagues"));

    for (let index = 0; index < user.leagues.length; index++) {

        for (let i = 0; i < leagues.length; i++) {
            if (leagues[i].id === user.leagues[index]) {
                for (let z = 0; z < leagues[i].activities.length; z++) {
                    for (let j = 0; j < allActivities.length; j++) {
                        if (allActivities[j].id === leagues[i].activities[z]) {
                            const activity = allActivities[j];
                            if (activity.confirmed.includes(user.id))
                                activities.push(activity);
                        }
                    }
                }
            }
        }

    }



    return activities;
}

function getLeagueActivities(league) {
    var activities = [];
    var allActivities = JSON.parse(localStorage.getItem("activities"));
    var leagues = JSON.parse(localStorage.getItem("leagues"));

    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i].id != league)
            continue;
        for (let z = 0; z < leagues[i].activities.length; z++) {
            for (let j = 0; j < allActivities.length; j++) {
                if (allActivities[j].id !== leagues[i].activities[z])
                    continue;
                const activity = allActivities[j];
                activities.push(activity);
            }
        }
    }

    return activities;
}

function newTeamID() {
    var teams = JSON.parse(localStorage.getItem("teams"));
    lastId = 0;
    for (var i = 0; i < teams.length; i++) {
        if (teams[i].id > lastId) {
            lastId = teams[i].id;
        }
    }
    return ++lastId;
}

function sportsAvailableInLeagues() {
    let sports = new Set();
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        let sport = leagues[i]["sportType"];
        sports.add(sport);
    }
    return sports;
}

function getLeague(leagueId) {
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i].id === leagueId) {
            return leagues[i];
        }
    }
    return null;
}

function removeAllChilds(id) {
    let myNode = document.getElementById(id);
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

// constructeur d'un utilisateur    (String, String, [string], [Object], [Object])
function User(id, password, teams, leagues) {
    this.id = id;
    this.password = password;
    this.teams = teams;
    this.leagues = leagues;
}

// constructeur d'une activité
function Activity(id, localisation, date, startHour, endHour, equipes, owner, confirmed, name) {
    this.id = id;
    this.name = name;
    this.localisation = localisation;
    this.startHour = startHour;
    this.endHour = endHour;
    this.date = date;
    this.equipes = equipes;
    this.owner = owner;
    this.confirmed = confirmed;
}

// constructeur d'une équipe    (String, String, Number, Number)
function Team(id, name, players, owner) {
    this.id = id;
    this.name = name;
    this.players = players;
    this.owner = owner;
}

// constructeur d'une ligue
function League(id, name, nbPlayersByTeams, nbTeams, seasonEnd, seasonStart, sportType, members, activities, teams, toApprove, owner) {
    this.id = id; // Number
    this.name = name; // String
    this.seasonStart = seasonStart; // String représentant une date
    this.seasonEnd = seasonEnd; // String représentant une date
    this.nbTeams = nbTeams; // Number
    this.nbPlayersByTeams = nbPlayersByTeams; // Number
    this.sportType = sportType; // String
    this.members = members; // Array de Number
    this.activities = activities; // Array de Number
    this.teams = teams; // Array de Number
    this.toApprove = toApprove; // Array de Number
    this.owner = owner; // Number
}

// var sport = sportEnum.VOLLEY;
// var sportSize = sportEnum.properties[sport].size;
var sportEnum = {
    VOLLEY: 1,
    ULTIMATE: 2,
    HOCKEY: 3,
    properties: {
        VOLLEY: { name: "beach volleyball", size: 6 },
        ULTIMATE: { name: "ultimate frisbee", size: 10 },
        HOCKEY: { name: "hockey cosom", size: 16 }
    }
};

var sportProperties = [
    { name: "beach volleyball", size: 6 },
    { name: "ultimate frisbee", size: 10 },
    { name: "hockey cosom", size: 16 }
];

function getSportType(teamId) {
    var leagues = JSON.parse(localStorage.getItem("leagues"));
    var sportType = ""
    if (leagues && leagues.length != 0) {
        for (let index = 0; index < leagues.length; index++) {
            const league = leagues[index];
            if (league.teams && league.teams.length != 0) {
                if (league.teams.indexOf(teamId) != -1) {
                    sportType = league.sportType;
                }
            }
        }
    }
    return sportType;
}

function getTeamName(id) {
    var teams = JSON.parse(localStorage.getItem("teams"));
    var name = "";
    if (teams && teams.length != 0) {
        for (let index = 0; index < teams.length; index++) {
            const team = teams[index];
            if (team.id == id) {
                name = team.name;
            }
        }
    }

    return name;

}


function idIsUnique() {
    let activityId = document.getElementById("activityId");
    let activities = JSON.parse(localStorage.getItem("activities"));

    if (!activityId && !activityId.value) {
        activityId.className = activityId.className + ' is-invalid'
        return false;
    }
    if (activities && activities.length != 0) {
        for (let index = 0; index < activities.length; index++) {
            const activity = activities[index];
            if (activity.id == activityId.value) {
                activityId.className = activityId.className + ' is-invalid'
                return false;
            }
        }
    }

    activityId.classList.remove('is-invalid');
    return true;
}