myLeagueTable();


function myLeagueTable() {

    // var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    var leagues = JSON.parse(localStorage.getItem("leagues"));

    if (leagues && leagues.length > 0) {

        var TableDiv = document.getElementById("league-table");

        var table = document.createElement('table');
        table.className = "table";

        var tableHeader = document.createElement('thead');

        var tr_head = document.createElement('tr');

        table.appendChild(tableHeader);

        tableHeader.appendChild(tr_head);

        var th_head_Titre = document.createElement('th');
        th_head_Titre.appendChild(document.createTextNode("Titre"));
        var th_head_Debut = document.createElement('th');
        th_head_Debut.appendChild(document.createTextNode("Debut"));
        var th_head_Fin = document.createElement('th');
        th_head_Fin.appendChild(document.createTextNode("Fin"));
        var th_head_NombreEquipe = document.createElement('th');
        th_head_NombreEquipe.className = "text-center";
        th_head_NombreEquipe.appendChild(document.createTextNode("Nombre Equipe"));
        var th_head_JoueursEquipe = document.createElement('th');
        th_head_JoueursEquipe.className = "text-center";
        th_head_JoueursEquipe.appendChild(document.createTextNode("Joueurs Equipe"));


        tr_head.appendChild(th_head_Titre);
        tr_head.appendChild(th_head_Debut);
        tr_head.appendChild(th_head_Fin);
        tr_head.appendChild(th_head_NombreEquipe);
        tr_head.appendChild(th_head_JoueursEquipe);
        if (currentUser && currentUser.id === "admin" ) {
            var tr_head_admin = document.createElement('th');
            tr_head_admin.className = "text-center";
            tr_head_admin.appendChild(document.createTextNode("Admin"));
            tr_head.appendChild(tr_head_admin);
        }

        var tableBody = document.createElement('tbody');
        table.appendChild(tableBody);

        for (var i = 0; i < leagues.length; i++) {
            var tr_body = document.createElement('tr');
            tr_body.className = leagues[i].id;
            tableBody.appendChild(tr_body);

            var title = leagues[i].name;
            var seasonStart = leagues[i].seasonStart;
            var seasonEnd = leagues[i].seasonEnd;
            var nbTeams = leagues[i].nbTeams;
            var nbPlayerByTeams = leagues[i].nbPlayersByTeams;


            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(title));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(seasonStart));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(seasonEnd));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(nbTeams));
            td.className = "text-center";
            tr_body.appendChild(td);

            var td = document.createElement('td');
            td.width = '75';
            td.appendChild(document.createTextNode(nbPlayerByTeams));
            td.className = "text-center";
            tr_body.appendChild(td);

            if (currentUser && currentUser.id === "admin" ) {
                var hrefSupprimer = document.createElement('a');
                hrefSupprimer.className = "btn btn-danger btn-xs";
                hrefSupprimer.href = "#";
                hrefSupprimer.innerHTML = "Supprimer";
                hrefSupprimer.addEventListener("click", function () { if (confirm("Êtes-vous sur?")); this.parentElement.parentElement.remove()});
                
                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(hrefSupprimer);
                td.className = "text-center";
                tr_body.appendChild(td);
            }

    

        }
        TableDiv.appendChild(table);
    } else {
        var p = document.createElement('p');
        p.innerHTML = "Aucune ligue";
        var corp = document.getElementById("league-table");
        corp.appendChild(p);
    }

    return true;
}
