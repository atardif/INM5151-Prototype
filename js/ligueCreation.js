function ligueCreation() {
    let leagueId = Number(document.getElementById("leagueId").value);
    let name = document.getElementById("leagueName").value;
    let seasonStart = document.getElementById("seasonStart").value;
    let seasonEnd = document.getElementById("seasonEnd").value;
    let nbTeams = document.getElementById("nbTeams").value;
    let nbPlayerByTeams = document.getElementById("nbPlayersByTeams").value;
    let sportType = document.getElementById("sportType").value;

    if ((Date.parse(seasonEnd) <= Date.parse(seasonStart))) {
        alert("Date de fin de saison doit etre superieur a la date de debut");
        return false;
    }

    if (!idIsUnique()) {
        return false;
    }

    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let listUsers = [];
    let newLeague = new League(
        leagueId, name, nbPlayerByTeams, nbTeams, seasonEnd, seasonStart,
        sportType, [currentUser.id], [], [], [], currentUser.id
    );

    if (newLeague !== null) {
        let leagues = JSON.parse(localStorage.getItem("leagues"));
        leagues.push(newLeague);
        localStorage.setItem("leagues", JSON.stringify(leagues));

        currentUser.leagues.push(leagueId);
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        listUsers = JSON.parse(localStorage.getItem("users"));

        for (let i = 0; i < listUsers.length; i++) {
            if (listUsers[i].id === currentUser.id) {
                listUsers[i].leagues.push(leagueId);
            }
        }

        localStorage.setItem("users", JSON.stringify(listUsers));
        return true;
    }
    return false;
}

function idIsUnique() {
    let leagueId = document.getElementById("leagueId");
    let leagues = JSON.parse(localStorage.getItem("leagues"));

    if (!leagueId && !leagueId.value) {
        leagueId.className = leagueId.className + ' is-invalid'
        return false;
    }
    if (leagues && leagues.length != 0) {
        for (let index = 0; index < leagues.length; index++) {
            const league = leagues[index];
            if (league.id == leagueId.value) {
                leagueId.className = leagueId.className + ' is-invalid'
                return false;
            }
        }
    }

    leagueId.classList.remove('is-invalid');
    return true;
}
