myActivityTable();


function myActivityTable() {

    var currentLeague = JSON.parse(localStorage.getItem("currentLeague"));
    if (currentLeague) {
        var activities = getLeagueActivities(currentLeague);


        if (activities.length > 0) {

            var myTableDiv = document.getElementById("my-activity-table");

            var table = document.createElement('table');
            table.className = "table";

            var tableHeader = document.createElement('thead');

            var tr_head = document.createElement('tr');

            table.appendChild(tableHeader);

            tableHeader.appendChild(tr_head);

            var th_head_Name = document.createElement('th');
            th_head_Name.appendChild(document.createTextNode("Name"))
            var th_head_Localisation = document.createElement('th');
            th_head_Localisation.appendChild(document.createTextNode("Localisation"));
            var th_head_startHour = document.createElement('th');
            th_head_startHour.appendChild(document.createTextNode("startHour"));
            var th_head_endHour = document.createElement('th');
            th_head_endHour.appendChild(document.createTextNode("endHour"));
            var th_head_date = document.createElement('th');
            th_head_date.appendChild(document.createTextNode("date"));
            var th_head_hostTeam = document.createElement('th');
            th_head_hostTeam.appendChild(document.createTextNode("hostTeam"));
            var th_head_guestTeam = document.createElement('th');
            th_head_guestTeam.appendChild(document.createTextNode("guestTeam"));
            var th_head_Actions = document.createElement('th');
            th_head_Actions.className = "text-center";
            th_head_Actions.appendChild(document.createTextNode("Actions"));

            tr_head.appendChild(th_head_Name);
            tr_head.appendChild(th_head_Localisation);
            tr_head.appendChild(th_head_startHour);
            tr_head.appendChild(th_head_endHour);
            tr_head.appendChild(th_head_date);
            tr_head.appendChild(th_head_hostTeam);
            tr_head.appendChild(th_head_guestTeam);
            tr_head.appendChild(th_head_Actions);

            var tableBody = document.createElement('tbody');
            table.appendChild(tableBody);


            for (var i = 0; i < activities.length; i++) {
                var tr_body = document.createElement('tr');
                tableBody.appendChild(tr_body);


                var name = activities[i].name;
                var localisation = activities[i].localisation;
                var startHour = activities[i].startHour;
                var endHour = activities[i].endHour;
                var date = activities[i].date;
                var hostTeam = activities[i].equipes[0];
				var guestTeam = activities[i].equipes[1];

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(name));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(localisation));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(startHour));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(endHour));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(date));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(getTeamName(hostTeam)));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(getTeamName(guestTeam)));
                tr_body.appendChild(td);

                var hrefConsulter = document.createElement('a');

                if (activities[i].confirmed.includes(currentUser.id)) {
                    hrefConsulter.className = "btn btn-danger btn-xs";
                    hrefConsulter.href = "#";
                    hrefConsulter.id = activities[i].id;
                    hrefConsulter.innerHTML = "Retirer la participation";
                    hrefConsulter.addEventListener("click", function () { modifyActivityParticipation(this.id); location.reload();});
                } else {
                    hrefConsulter.className = "btn btn-info btn-xs";
                    hrefConsulter.href = "#";
                    hrefConsulter.id = activities[i].id;
                    hrefConsulter.innerHTML = "Confirmer la participation";
                    hrefConsulter.addEventListener("click", function () { modifyActivityParticipation(this.id); location.reload();});
                    //hrefConsulter.style = "margin-right:5px;";
                }


                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(hrefConsulter);
                td.className = "text-center";
                tr_body.appendChild(td);

            }
            myTableDiv.appendChild(table);
        } else {
            var p = document.createElement('p');
            p.innerHTML = "Vous n'avez aucune activité de prévue";
            var corp = document.getElementById("my-activity-table");
            corp.appendChild(p);
        }
    }
    return true;
}

function modifyActivityParticipation(id) {
        var activities = JSON.parse(localStorage.getItem("activities"));
        for (var i = 0; i < activities.length; i++) {
            if (activities[i].id == id)
                if (activities[i].confirmed.includes(currentUser.id)) 
                    activities[i].confirmed = activities[i].confirmed.filter(e => e !== currentUser.id)
                else
                    activities[i].confirmed.push(currentUser.id);
        }
        localStorage.setItem("activities", JSON.stringify(activities));
}
