myTeamTable();

function myTeamTable() {

    var currentLeague = JSON.parse(localStorage.getItem("currentLeague"));
    var leagues = JSON.parse(localStorage.getItem("leagues"));
    var teams = JSON.parse(localStorage.getItem("teams"));
    var league;

    for (var i = 0; i < leagues.length; i++)
        if (leagues[i].id === currentLeague)
            league = leagues[i];

    if (league && league.teams.length > 0) {

        var myTableDiv = document.getElementById("my-team-table");

        var table = document.createElement('table');
        table.className = "table";

        var tableHeader = document.createElement('thead');

        var tr_head = document.createElement('tr');

        table.appendChild(tableHeader);

        tableHeader.appendChild(tr_head);

        var th_head_Nom = document.createElement('th');
        th_head_Nom.appendChild(document.createTextNode("Nom"));
        var th_head_Sport = document.createElement('th');
        th_head_Sport.appendChild(document.createTextNode("Sport"));
        var th_head_MembreEquipe = document.createElement('th');
        th_head_MembreEquipe.appendChild(document.createTextNode("Membre de l'equipe"));
        var th_head_Actions = document.createElement('th');
        th_head_Actions.className = "text-center";
        th_head_Actions.appendChild(document.createTextNode("Actions"));

        tr_head.appendChild(th_head_Nom);
        tr_head.appendChild(th_head_Sport);
        tr_head.appendChild(th_head_MembreEquipe);
        tr_head.appendChild(th_head_Actions);

        var tableBody = document.createElement('tbody');
        table.appendChild(tableBody);

        for (var z = 0; z < teams.length; z++) {
            for (var j = 0; j < league.teams.length; j++) {
                if (teams[z].id !== league.teams[j])
                    continue;
                var tr_body = document.createElement('tr');
                tableBody.appendChild(tr_body);

                var titleTeam = teams[z].name;
                var sportType = getSportType(teams[z].id);

                var playersTable = "";
                for (var j = 0; j < teams[z].players.length; j++) {
                    playersTable += teams[z].players[j] + " ";
                }


                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(titleTeam));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(sportType));
                tr_body.appendChild(td);

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(document.createTextNode(playersTable));
                tr_body.appendChild(td);

                var hrefConsulter = document.createElement('a');
                //var hrefSupprimer = document.createElement('a');

                if (teams[z].players.includes(currentUser.id)) {
                    hrefConsulter.className = "btn btn-danger btn-xs";
                    hrefConsulter.href = "#";
                    hrefConsulter.innerHTML = "Quitter";
                    hrefConsulter.id = teams[z].id;
                    hrefConsulter.addEventListener("click", function () { modifyTeamParticipation(this.id); location.reload(); });
                } else {
                    hrefConsulter.className = "btn btn-info btn-xs";
                    hrefConsulter.href = "#";
                    hrefConsulter.innerHTML = "Rejoindre";
                    hrefConsulter.id = teams[z].id;
                    hrefConsulter.addEventListener("click", function () { modifyTeamParticipation(this.id); location.reload(); });
                    //hrefConsulter.style = "margin-right:5px;";
                }

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(hrefConsulter);
                td.className = "text-center";
                tr_body.appendChild(td);
            }
        }
        myTableDiv.appendChild(table);
    } else {
        var p = document.createElement('p');
        p.innerHTML = "Vous n'êtes présentement membre d'aucune équipe";
        var corp = document.getElementById("my-team-table");
        corp.appendChild(p);
    }

    return true;

}

function modifyTeamParticipation(id) {
    var teamid = parseInt(id);
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    var users = JSON.parse(localStorage.getItem("users"));
    currentUser.teams.push(teamid);
    if (currentUser.teams.includes(teamid))
        currentUser.teams = currentUser.teams.filter(e => e != id)
    else
        currentUser.teams.push(id);
    for (var i = 0; i < users.length; i++)
        if (users[i].id === currentUser.id)
            if (users[i].teams.includes(teamid))
                users[i].teams = users[i].teams.filter(e => e != id)
            else
                users[i].teams.push(teamid);


    localStorage.setItem("currentUser", JSON.stringify(currentUser));
    localStorage.setItem("users", JSON.stringify(users));

    var teams = JSON.parse(localStorage.getItem("teams"));
    for (var i = 0; i < teams.length; i++) {
        if (teams[i].id == id)
            if (teams[i].players.includes(currentUser.id))
                teams[i].players = teams[i].players.filter(e => e !== currentUser.id)
            else
                teams[i].players.push(currentUser.id);
    }
    localStorage.setItem("teams", JSON.stringify(teams));
}