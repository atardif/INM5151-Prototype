let leagueUsers = [];
let teamUsers = [];

loadLeagues();
loadUsersInSelectedLeague();

function loadLeagues() {
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    let leagueSet = new Set();
    for (let i = 0; i < leagues.length; i++) {
        for (let j = 0; j < leagues[i].members.length; j++) {
            if (leagues[i].members[j] === currentUser.id) {
                if (!leagueSet.has(leagues[i].id)) {
                    leagueSet.add(leagues[i].id);
                    $('#list-league').append(new Option(leagues[i].name, leagues[i].id, false, false));
                }

            }
        }
    }
}

function loadUsersInSelectedLeague() {
    let currentLeagueId = Number($("#list-league").val());
    let allUsers = JSON.parse(localStorage.getItem("users"));
    leagueUsers = [];
    teamUsers = [];
    removeAllChilds("participants-team-list");
    removeAllChilds("my-team-list");
    let league = getLeague(currentLeagueId);
    let members = league["members"];
    for (let j = 0; j < members.length; j++) {
        let userId = members[j];
        for (let k = 0; k < allUsers.length; k++) {
            let user = allUsers[k];
            if (user.id === userId) {
                leagueUsers.push(user);
            }
        }
    }

    if (leagueUsers.length > 0) {
        let corp = document.getElementById("participants-team-list");
        for (let i = 0; i < leagueUsers.length; i++) {
            let a = document.createElement('a');  
            a.className = 'list-group-item list-group-item-warning list-group-item-action';
            a.innerHTML = leagueUsers[i].id;
            a.setAttribute('onclick', 'addToMyTeam(\'' + leagueUsers[i].id + '\');');
            a.id = leagueUsers[i].id;
            corp.appendChild(a);
        }
    } else {
        let p = document.createElement('h5');
        p.innerHTML = "Liste des participants est vide";
        let corp = document.getElementById("participants-team-list");
        corp.appendChild(p);
    }
    return true;
}

function addToMyTeam(argument) {
    for (let i = 0; i < leagueUsers.length; i++) {
        if (leagueUsers[i].id === argument) {
            leagueUsers.splice(i, 1);
        }
    }

    document.getElementById(argument).outerHTML = "";

    if (leagueUsers.length < 1) {
        let p = document.createElement('h5');
        p.innerHTML = "Liste des participants est vide";
        let corp = document.getElementById("participants-team-list");
        corp.appendChild(p);
    }

    teamUsers.push(argument);

    let corp = document.getElementById("my-team-list");
    let a = document.createElement('a');  
    a.className = 'list-group-item list-group-item-success';
    a.innerHTML = argument;
    a.id = argument;
    corp.appendChild(a);

    if (teamUsers.length > 0) {
        let element =  document.getElementById('my_team_title');
        if (typeof(element) !== 'undefined' && element !== null) {
            document.getElementById("my_team_title").remove();
        }
    }
    return true;
}

function teamCreation() {
    if (teamUsers.length === 0) {
        return false;
    }

    let name = document.getElementById("champ-team").value;
    let currentLeagueId = Number($("#list-league").val());
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let id = newTeamID();
    let newTeam = new Team(id, name, teamUsers, currentUser.id);

    if (newTeam !== null) {
        // team
        let teams = JSON.parse(localStorage.getItem("teams"));
        teams.push(newTeam);
        localStorage.setItem("teams", JSON.stringify(teams));	

        // members
        currentUser.teams.push(id);
        localStorage.setItem("currentUser", JSON.stringify(currentUser));

        let listUsers = JSON.parse(localStorage.getItem("users"));
        for (let i = 0; i < listUsers.length; i++) {
            for (let j = 0; j < teamUsers.length; j++) {
                if (listUsers[i].id === teamUsers[j]) {
                    listUsers[i].teams.push(id);
                }
            }
        }
        localStorage.setItem("users", JSON.stringify(listUsers));	 

        // league
        let leagues = JSON.parse(localStorage.getItem("leagues"));
        let leagueIndex = -1;
        let league;
        for (let i = 0; i < leagues.length; i++) {
            if (leagues[i].id === currentLeagueId) {
                league = leagues[i];
                leagueIndex = i;
            }
        }
        if (leagueIndex !== -1) {
            league["teams"].push(id);
            leagues[leagueIndex] = league;
            localStorage.setItem("leagues", JSON.stringify(leagues));
        }
  
        return true;
    }
    return false;
}
