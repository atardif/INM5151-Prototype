myActivityTable();


function myActivityTable() {

	var currentUser = JSON.parse(localStorage.getItem("currentUser"));
	if (currentUser) {
        var ligues = JSON.parse(localStorage.getItem("leagues"));
        var currentLeague;
        for (var i = 0; i < ligues.length; i++) {
            if (ligues[i].id == localStorage.getItem("currentLeague"))
                currentLeague = ligues[i];
        }

        if (currentLeague.owner !== currentUser.id)
            return;
        

            var myOuterDiv = document.getElementById("admin-panel");
            
            var myCardDiv = document.createElement('div');
            myCardDiv.className = "card";
            myOuterDiv.appendChild(myCardDiv);

            var myCardHeaderDiv = document.createElement('div');
            myCardHeaderDiv.className = "card-header"
            myCardDiv.appendChild(myCardHeaderDiv);

            var myCardTitle = document.createElement('h3');
            myCardTitle.className = "card-title text-center" 
            myCardTitle.innerText = "Admin"
            myCardHeaderDiv.appendChild(myCardTitle);

            var myTableDiv = document.createElement('div');
            myTableDiv.className = "card-body";
            myCardDiv.appendChild(myTableDiv);


		if (currentLeague.toApprove.length > 0) {


			var table = document.createElement('table');
			table.className = "table";

			var tableHeader = document.createElement('thead');

			var tr_head = document.createElement('tr');

			table.appendChild(tableHeader);

			tableHeader.appendChild(tr_head);

			var th_head_Name = document.createElement('th');
			th_head_Name.appendChild(document.createTextNode("Name"))	
			var th_head_Actions = document.createElement('th');
			th_head_Actions.className = "text-center";
			th_head_Actions.appendChild(document.createTextNode("Actions"));

			tr_head.appendChild(th_head_Name);
			tr_head.appendChild(th_head_Actions);

			var tableBody = document.createElement('tbody');
			table.appendChild(tableBody);


			for (var i = 0; i < currentLeague.toApprove.length; i++) {
				var tr_body = document.createElement('tr');
				tableBody.appendChild(tr_body);


				var name = currentLeague.toApprove[i];

				var td = document.createElement('td');
				td.width = '75';
				td.appendChild(document.createTextNode(name));
				tr_body.appendChild(td);

				var hrefAccepter = document.createElement('a');
				var hrefRefuser = document.createElement('a');

				hrefAccepter.className = "btn btn-info btn-xs";
				hrefAccepter.href = "#";
				hrefAccepter.innerHTML = "Accepter";
                hrefAccepter.style = "margin-right:5px;";
                hrefAccepter.addEventListener("click", function () { acceptMember(this.parentElement.parentElement.children[0].innerHTML) });

				hrefRefuser.className = "btn btn-danger btn-xs";
				hrefRefuser.href = "#";
                hrefRefuser.innerHTML = "Refuser";
                hrefRefuser.addEventListener("click", function () { if (confirm("Êtes-vous sur?")) refuseMember(this.parentElement.parentElement.children[0].innerHTML) });

				var td = document.createElement('td');
				td.width = '75';
				td.appendChild(hrefAccepter);
				td.appendChild(hrefRefuser);
				td.className = "text-center";
				tr_body.appendChild(td);

			}
			myTableDiv.appendChild(table);
		} else {
			var p = document.createElement('p');
			p.innerHTML = "Aucun nouveau membres";
			myTableDiv.appendChild(p);
		}
	}
	return true;
}
