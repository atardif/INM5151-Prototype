loadTeams();
loadActivitesForSelectedTeam();

function getLeagueForTeam(teamId) {
    let leagues = JSON.parse(localStorage.getItem("leagues"));
    for (let i = 0; i < leagues.length; i++) {
        if (leagues[i]["teams"].includes(teamId)) {
            return leagues[i];
        }
    }
    return null;
}

function loadTeams() {
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    let teams = JSON.parse(localStorage.getItem("teams"));
    let userTeams = teams.filter(team => team["players"].includes(currentUser.id));
    removeAllChilds("list-teams");
    for (let i = 0; i < userTeams.length; i++) {
        $('#list-teams').append(new Option(userTeams[i].name, userTeams[i].id, false, false));
    }
}

function loadActivitesForSelectedTeam() {
    let currentTeamId = Number($("#list-teams").val());
    let allActivities = JSON.parse(localStorage.getItem("activities"));
    let league = getLeagueForTeam(currentTeamId);
    removeAllChilds("list-activities");
    let availableActivities = allActivities.filter(
        activity => league["activities"].includes(activity["id"]) && !activity["equipes"].includes(currentTeamId)
    );

    for (let i = 0; i < availableActivities.length; i++) {
        let activity = availableActivities[i];
        let name = "Activité: " + activity["name"];
        $('#list-activities').append(new Option(name, activity["id"], false, false));
    }
}

function joinActivity() {
    let currentTeamId = Number($("#list-teams").val());
    let currentActivityId = Number($("#list-activities").val());
    let league = getLeagueForTeam(currentTeamId);

    if (currentTeamId && currentActivityId) {
        // update activities
        let activities = JSON.parse(localStorage.getItem("activities"));
        let i = 0
        while (i < activities.length) {
            if (activities[i]["id"] === currentActivityId) {
                activities[i]["equipes"].push(currentTeamId);
                activities[i]["confirmed"].push(currentUser.id)
                league.activities.push(currentActivityId);
                break;
            }
            i++;
        }
        localStorage.setItem("activities", JSON.stringify(activities));
        localStorage.setItem("leagues", JSON.stringify(leagues));
        return true;
    }
    return false;
}
