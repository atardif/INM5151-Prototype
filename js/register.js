// fonction validant des champs requis et rajoutant un utilisateur dans le storage.
function validateRegister() {
    var usernameIsValid = validateInput("champ_username");
    var passwordIsValid = validateInput("champ_password");
    var username = document.getElementById("champ_username").value;
    var password = document.getElementById("champ_password").value;

    var usersList = [];


    if (usernameIsValid && passwordIsValid) {
        var currentUser = new User(username, password, [], [], []);
        
        usersList = JSON.parse(localStorage.getItem("users")); 
        usersList.push(currentUser);
        localStorage.setItem("users", JSON.stringify(usersList));
        localStorage.setItem("currentUser", JSON.stringify(currentUser));            
        showButtons();
        return true
    }

    return false;
}