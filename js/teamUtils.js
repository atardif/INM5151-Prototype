function deleteTeam(teamName) {
    var user = JSON.parse(localStorage.getItem("currentUser"));
    var usersList = JSON.parse(localStorage.getItem("users")); 
    for (var i = 0; i < usersList.length; i++) {
        for (var j = 0; j < usersList[i].teams.length; j++) {
            if (usersList[i].teams[j].name === teamName) {
                usersList[i].teams.splice(j,1);
            }
        }
    }
    for (var j = 0; j < user.teams.length; j++) {
        if (user.teams[j].name === teamName) {
            user.teams.splice(j,1);
        }
    }
    localStorage.setItem("currentUser", JSON.stringify(user));	
    localStorage.setItem("users", JSON.stringify(usersList));
}
