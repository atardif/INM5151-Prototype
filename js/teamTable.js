myTeamTable();

function myTeamTable() {

    var teams = JSON.parse(localStorage.getItem("teams"));

    if (teams && teams.length > 0) {

        var myTableDiv = document.getElementById("team-table");

        var table = document.createElement('table');
        table.className = "table";

        var tableHeader = document.createElement('thead');

        var tr_head = document.createElement('tr');

        table.appendChild(tableHeader);

        tableHeader.appendChild(tr_head);

        var th_head_Nom = document.createElement('th');
        th_head_Nom.appendChild(document.createTextNode("Nom"));
        var th_head_Sport = document.createElement('th');
        th_head_Sport.appendChild(document.createTextNode("Sport"));
        var th_head_MembreEquipe = document.createElement('th');
        th_head_MembreEquipe.appendChild(document.createTextNode("Membre de l'equipe"));

        if (currentUser && currentUser.id === "admin" ) {
            var th_head_Supprimer = document.createElement('th');
            th_head_Supprimer.appendChild(document.createTextNode("Action"));            
        } 


        tr_head.appendChild(th_head_Nom);
        tr_head.appendChild(th_head_Sport);
        tr_head.appendChild(th_head_MembreEquipe);

        if (currentUser && currentUser.id === "admin" ) {
            tr_head.appendChild(th_head_Supprimer);    
        }

        var tableBody = document.createElement('tbody');
        table.appendChild(tableBody);


        for (var i = 0; i < teams.length; i++) {
            var team = teams[i];
            var tr_body = document.createElement('tr');
            tableBody.appendChild(tr_body);

            var titleTeam = team.name;
            var sportType = team.sportType;

            var playersTable = "";
            if (team.players && team.players.length != 0) {
                for (var j = 0; j < team.players.length; j++) {
                    playersTable += team.players[j] + " ";
                }
            }


            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(titleTeam));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(getSportType(team.id)));
            tr_body.appendChild(td);

            var td = document.createElement('td');
            //td.width = '300';
            td.appendChild(document.createTextNode(playersTable));
            tr_body.appendChild(td);
  
            var hrefSupprimer = document.createElement('a');

            if (currentUser && currentUser.id === "admin" ) {
                hrefSupprimer.className = "btn btn-danger btn-xs";
                hrefSupprimer.href = "#";
                hrefSupprimer.innerHTML = "Supprimer";
                hrefSupprimer.addEventListener("click", function () { if (confirm("Êtes-vous sur?")); this.parentElement.parentElement.remove()});

                var td = document.createElement('td');
                td.width = '75';
                td.appendChild(hrefSupprimer);
                td.className = "text-center";
                tr_body.appendChild(td);
            } 
        }

        myTableDiv.appendChild(table);
    } else {
        var p = document.createElement('p');
        p.innerHTML = "Aucune équipe";
        var corp = document.getElementById("team-table");
        corp.appendChild(p);
    }

    return true;

}

function removeTeam(element) {
    deleteTeam(element.children[0].innerHTML)
    element.remove();
}

